package com.bar.drinkup.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class UsersDbManager {
    private static final String TAG = UsersDbManager.class.getSimpleName();

    private static UsersDbManager instance;
    private final DbHelper dbHelper;

    private UsersDbManager() {
        dbHelper = new DbHelper();
    }

    public static synchronized UsersDbManager getInstance() {
        if (instance == null) {
            instance = new UsersDbManager();
        }
        return instance;
    }

    public ArrayList<SaveOrder> getAllUsers() {
        ArrayList<SaveOrder> allUsers = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DbHelper.DB_TABLE_NAME, null, null, null, null, null, null);

        if (c.moveToFirst()) {
            int mName = c.getColumnIndex(DbHelper.DB_COLUMN_CUSTOMER_NAME);
            int mEmail = c.getColumnIndex(DbHelper.DB_COLUMN_CUSTOMER_EMAIL);
            int mPhone = c.getColumnIndex(DbHelper.DB_COLUMN_CUSTOMER_PHONE);
            int mDrink = c.getColumnIndex(DbHelper.DB_COLUMN_CUSTOMER_DRINK);

            do {
                SaveOrder qbUser = new SaveOrder();
                qbUser.setName(c.getString(mName));
                qbUser.setEmail(c.getString(mEmail));
                qbUser.setPhone(c.getString(mPhone));
                qbUser.setDrink(c.getString(mDrink));
                allUsers.add(qbUser);
            } while (c.moveToNext());
        }

        c.close();
        dbHelper.close();

        return allUsers;
    }

    public void saveUser(SaveOrder qbUser) {
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        cv.put(DbHelper.DB_COLUMN_CUSTOMER_NAME, qbUser.getName());
        cv.put(DbHelper.DB_COLUMN_CUSTOMER_EMAIL, qbUser.getEmail());
        cv.put(DbHelper.DB_COLUMN_CUSTOMER_PHONE, qbUser.getPhone());
        cv.put(DbHelper.DB_COLUMN_CUSTOMER_DRINK, qbUser.getDrink());

        db.insert(DbHelper.DB_TABLE_NAME, null, cv);
        dbHelper.close();
    }

    public void clearDB() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(DbHelper.DB_TABLE_NAME, null, null);
        dbHelper.close();
    }
}