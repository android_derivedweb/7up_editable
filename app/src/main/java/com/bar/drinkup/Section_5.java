package com.bar.drinkup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bar.drinkup.db.SaveOrder;
import com.bar.drinkup.db.UsersDbManager;

public class Section_5 extends AppCompatActivity {

    private UsersDbManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sec_5);

        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finaly change the color
        window.setStatusBarColor(ContextCompat.getColor(Section_5.this, R.color.teal_200));

        dbManager = UsersDbManager.getInstance();

        Bundle extras = getIntent().getExtras();
        String mName = extras.getString("mName");
        String mMobile = extras.getString("mMobile");
        String mEmail = extras.getString("mEmail");
        String mDrink = extras.getString("mDrink");

        findViewById(R.id.mSifuiente).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SaveOrder saveOrder = new SaveOrder();
                saveOrder.setName(mName);
                saveOrder.setEmail(mEmail);
                saveOrder.setPhone(mMobile);
                saveOrder.setDrink(mDrink);
                dbManager.saveUser(saveOrder);

                startActivity(new Intent(Section_5.this, Section_6.class)
                        .putExtra("mName", mName)
                        .putExtra("mMobile", mMobile)
                        .putExtra("mEmail", mEmail)
                        .putExtra("mDrink", mDrink)
                );
                finish();
            }
        });

    }


}