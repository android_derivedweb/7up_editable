package com.bar.drinkup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bar.drinkup.db.UsersDbManager;


public class Section_4 extends AppCompatActivity {

    private TextView title1, title2, title4, title5;
    private TextView dec1, dec2, dec4, dec5;

    private LinearLayout mLayout1, mLayout2, mLayout4, mLayout5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sec_4);

        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finaly change the color
        window.setStatusBarColor(ContextCompat.getColor(Section_4.this, R.color.teal_200));

        // AndroidNetworking.initialize(getApplicationContext());

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title4 = findViewById(R.id.title4);
        title5 = findViewById(R.id.title5);

        dec1 = findViewById(R.id.dec1);
        dec2 = findViewById(R.id.dec2);
        dec4 = findViewById(R.id.dec4);
        dec5 = findViewById(R.id.dec5);

        mLayout1 = findViewById(R.id.mLayout1);
        mLayout2 = findViewById(R.id.mLayout2);
        mLayout4 = findViewById(R.id.mLayout4);
        mLayout5 = findViewById(R.id.mLayout5);


        Bundle extras = getIntent().getExtras();
        String mName = extras.getString("mName");
        String mMobile = extras.getString("mMobile");
        String mEmail = extras.getString("mEmail");
//        String mName = "name test";
//        String mMobile = "1010101010";
//        String mEmail = "john@gmail.com";

        mLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToNext(mName, mMobile, mEmail, title1.getText().toString());
            }
        });
        mLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToNext(mName, mMobile, mEmail, title2.getText().toString());
            }
        });

        mLayout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToNext(mName, mMobile, mEmail, title4.getText().toString());
            }
        });
        mLayout5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToNext(mName, mMobile, mEmail, title5.getText().toString());
            }
        });

    }


    private void moveToNext(String mName, String mMobile, String mEmail, String mDrink) {

        startActivity(new Intent(Section_4.this, Section_5.class)
                .putExtra("mName", mName)
                .putExtra("mMobile", mMobile)
                .putExtra("mEmail", mEmail)
                .putExtra("mDrink", mDrink)
        );
        finish();

    }

}