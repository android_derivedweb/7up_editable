package com.bar.drinkup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

public class Section_1 extends AppCompatActivity {

    private EditText mName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sec_1);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(Section_1.this, R.color.teal_200));

        mName = findViewById(R.id.mName);

        findViewById(R.id.mSifuiente).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mName.getText().toString().isEmpty()) {
                    Toast.makeText(Section_1.this, "Enter name", Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(new Intent(Section_1.this, Section_2.class)
                            .putExtra("mName", mName.getText().toString()));
//                    finish();
                }

            }
        });


//        if (ContextCompat.checkSelfPermission(Section_1.this, android.Manifest.permission.BLUETOOTH_CONNECT) == PackageManager.PERMISSION_DENIED) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
//                ActivityCompat.requestPermissions(Section_1.this, new String[]{android.Manifest.permission.BLUETOOTH_CONNECT}, 2);
//                return;
//            }
//        }

        findViewById(R.id.btn_record).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogPin();
            }
        });

    }


    private void dialogPin() {
        final Dialog dialog = new Dialog(Section_1.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_pin);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        EditText mEmail = dialog.findViewById(R.id.mEmail);

        mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 5) {
                    if (s.toString().equalsIgnoreCase("Taste")) {
                        startActivity(new Intent(Section_1.this, RecordActivity.class));
                        dialog.dismiss();
                    } else {
                        Toast.makeText(Section_1.this, "Contraseña incorrecta", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        dialog.show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mName.setText("");

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 2: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //  Toast.makeText(Section_1.this, "Permission denied please allow from setting", Toast.LENGTH_SHORT).show();
                    //  finish();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}