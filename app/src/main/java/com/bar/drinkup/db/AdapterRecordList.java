
package com.bar.drinkup.db;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bar.drinkup.R;


import java.util.ArrayList;

public class AdapterRecordList extends RecyclerView.Adapter<AdapterRecordList.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<SaveOrder> timeSlotModelArrayList;

    public AdapterRecordList(Context mContext, ArrayList<SaveOrder> notificatioModelArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.timeSlotModelArrayList = notificatioModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_details, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });
        holder.mName.setText(timeSlotModelArrayList.get(position).getName());
        holder.mPhone.setText(timeSlotModelArrayList.get(position).getPhone());
        holder.mEmail.setText(timeSlotModelArrayList.get(position).getEmail());
        holder.mDrink.setText(timeSlotModelArrayList.get(position).getDrink());

    }

    @Override
    public int getItemCount() {
        return timeSlotModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        private TextView mName;
        private TextView mPhone;
        private TextView mEmail;
        private TextView mDrink;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            mName = itemView.findViewById(R.id.mName);
            mPhone = itemView.findViewById(R.id.mPhone);
            mEmail = itemView.findViewById(R.id.mEmail);
            mDrink = itemView.findViewById(R.id.mDrink);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

}