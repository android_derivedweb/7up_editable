package com.bar.drinkup;

import static android.os.Environment.DIRECTORY_DOWNLOADS;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bar.drinkup.db.AdapterRecordList;
import com.bar.drinkup.db.SaveOrder;
import com.bar.drinkup.db.UsersDbManager;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class RecordActivity extends AppCompatActivity {

    private UsersDbManager dbManager;
    private RecyclerView mRecyclerViewRecord;
    private AdapterRecordList mAdapterRecordList;
    private ArrayList<SaveOrder> dbArrayList;

    public static String[] storge_permissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    @RequiresApi(api = Build.VERSION_CODES.TIRAMISU)
    public static String[] storge_permissions_33 = {
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.READ_MEDIA_AUDIO,
            Manifest.permission.READ_MEDIA_VIDEO
    };

    public static String[] permissions() {
        String[] p;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            p = storge_permissions_33;
        } else {
            p = storge_permissions;
        }
        return p;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_activity);

        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finaly change the color
        window.setStatusBarColor(ContextCompat.getColor(RecordActivity.this, R.color.teal_200));

        dbManager = UsersDbManager.getInstance();
        dbArrayList = dbManager.getAllUsers();

        if (dbManager.getAllUsers().isEmpty()){
            findViewById(R.id.noDataTV).setVisibility(View.VISIBLE);
        }


        Log.e("ASdasd",dbManager.getAllUsers().size() + "");
        ActivityCompat.requestPermissions(RecordActivity.this,
                permissions(),
                1);

        mRecyclerViewRecord = findViewById(R.id.mRecyclerViewRecord);
        mRecyclerViewRecord.setLayoutManager(new LinearLayoutManager(RecordActivity.this));
        mAdapterRecordList = new AdapterRecordList(this, dbArrayList, new AdapterRecordList.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        mRecyclerViewRecord.setAdapter(mAdapterRecordList);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alert = new AlertDialog.Builder(RecordActivity.this);
                alert.setTitle("Delete entry");
                alert.setMessage("Are you sure you want to delete?");
                alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                        dbManager.clearDB();
                        if (dbManager.getAllUsers().isEmpty()){
                            findViewById(R.id.noDataTV).setVisibility(View.VISIBLE);
                        }
                        mAdapterRecordList = new AdapterRecordList(RecordActivity.this, dbManager.getAllUsers(), new AdapterRecordList.OnItemClickListener() {
                            @Override
                            public void onItemClick(int item) {

                            }
                        });
                        mRecyclerViewRecord.setAdapter(mAdapterRecordList);
                    }
                });
                alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // close dialog
                        dialog.cancel();
                    }
                });
                alert.show();
            }
        });
        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if(checkAndRequestPermissions()){




                }else {
                    Toast.makeText(RecordActivity.this, "Go to Setting and Approve Storage Permission", Toast.LENGTH_SHORT).show();
                }
*/
                createExcelSheet();
            }
        });


    }

    File  dir, file;
    WritableWorkbook workbook;

    void createExcelSheet() {

        String  timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());

        String csvFile = "7up_Tragops_"+timeStamp+".xls";
        if (Build.VERSION_CODES.R > Build.VERSION.SDK_INT) {
            dir = new File(Environment.getExternalStorageDirectory().getPath()
                    + "//"+getResources().getString(R.string.app_name));
        } else {
            dir = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS).getPath()
                    + "//"+getResources().getString(R.string.app_name));
        }

        if (!dir.exists())
            dir.mkdir();
        file = new File(dir, csvFile);
        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("en", "EN"));
        try {
            workbook = Workbook.createWorkbook(file, wbSettings);
            createFirstSheet();
            //closing cursor
            workbook.write();
            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Toast.makeText(RecordActivity.this, "File Path : " + dir.getPath() +csvFile, Toast.LENGTH_SHORT).show();

    }

    void createFirstSheet() {
        try {
            ArrayList<SaveOrder> listdata = dbManager.getAllUsers();


            //Excel sheet name. 0 (number)represents first sheet
            WritableSheet sheet = workbook.createSheet("sheet1", 0);
            // column and row title
            sheet.addCell(new Label(0, 0, "Name"));
            sheet.addCell(new Label(1, 0, "Email"));
            sheet.addCell(new Label(2, 0, "Phone"));
            sheet.addCell(new Label(3, 0, "Drink"));

            for (int i = 0; i < listdata.size(); i++) {
                sheet.addCell(new Label(0, i + 1, listdata.get(i).getName()));
                sheet.addCell(new Label(1, i + 1, listdata.get(i).getEmail()));
                sheet.addCell(new Label(2, i + 1, listdata.get(i).getPhone()));
                sheet.addCell(new Label(3, i + 1, listdata.get(i).getDrink()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 7;

    private boolean checkAndRequestPermissions() {

        int wtite = ContextCompat.checkSelfPermission(RecordActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(RecordActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(RecordActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("in fragment on request", "Permission callback called-------");

        switch (requestCode) {

            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted

                        final File destination;
                        if (Build.VERSION_CODES.R > Build.VERSION.SDK_INT) {
                            destination = new File(Environment.getExternalStorageDirectory().getPath()
                                    + "//" + getResources().getString(R.string.app_name));
                        } else {
                            destination = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS).getPath()
                                    + "//" + getResources().getString(R.string.app_name));
                        }

                        if (!destination.exists())
                            destination.mkdir();

                    } else {
                        Log.d("in fragment on request", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(RecordActivity.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(RecordActivity.this, Manifest.permission.CAMERA) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(RecordActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Camera and Storage Permission required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(RecordActivity.this, getResources().getString(R.string.go_to_enable), Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(RecordActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }



}