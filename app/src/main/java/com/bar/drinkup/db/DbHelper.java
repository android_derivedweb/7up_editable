package com.bar.drinkup.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bar.drinkup.Myapplication;


public class DbHelper extends SQLiteOpenHelper {
    private String TAG = DbHelper.class.getSimpleName();

    private static final String DB_NAME = "7up_Tragops";

    public static final String DB_TABLE_NAME = "customer";
    public static final String DB_COLUMN_ID = "ID";
    public static final String DB_COLUMN_CUSTOMER_NAME = "name";
    public static final String DB_COLUMN_CUSTOMER_EMAIL = "email";
    public static final String DB_COLUMN_CUSTOMER_PHONE = "phone";
    public static final String DB_COLUMN_CUSTOMER_DRINK = "drink";

    public DbHelper() {
        super(Myapplication.getInstance(), DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "--- onCreate database ---");
        db.execSQL("create table " + DB_TABLE_NAME + " ("
                + DB_COLUMN_ID + " integer primary key autoincrement,"
                + DB_COLUMN_CUSTOMER_NAME + " text,"
                + DB_COLUMN_CUSTOMER_EMAIL + " text,"
                + DB_COLUMN_CUSTOMER_PHONE + " text,"
                + DB_COLUMN_CUSTOMER_DRINK + " text"
                + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}